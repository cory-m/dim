import torch
import numpy as np
import random

torch.manual_seed(1234)
torch.cuda.manual_seed_all(1234)
np.random.seed(1234)
random.seed(1234)

import argparse
import os
import time
import itertools
import pdb
import logging
from tensorboardX import SummaryWriter
from sklearn.metrics.cluster import normalized_mutual_info_score
from sklearn.metrics.cluster import adjusted_rand_score
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import yaml

import models
from utils.util import create_logger, AverageMeter, Logger, clustering_acc, WeightedBCE, accuracy, save_checkpoint, load_checkpoint
from utils.sampling import get_pair
from utils.mc_dataset import McDataset
from utils.functions import get_dim, forward, comp_simi 

# argparser
parser = argparse.ArgumentParser(description='PyTorch Implementation of DIM')
parser.add_argument('--resume', default=None, type=str, help='resume from a checkpoint')
parser.add_argument('--config', default='cfgs/config.yaml', help='set configuration file')
parser.add_argument('--bs', default=128, type=int)
parser.add_argument('--eval-cluster', action='store_true')

args = parser.parse_args()
with open(args.config) as f:
	config = yaml.load(f, Loader=yaml.FullLoader)
for k, v in config.items():
	setattr(args, k, v)

best_acc = 0
start_epoch = 0

def main():
	global args, best_acc, start_epoch
	
	os.makedirs('{}'.format(args.save_path), exist_ok=True)

	# logging configuration
	logger = create_logger('global_logger', log_file=os.path.join(args.save_path,'log.txt'))
	logger.info('{}'.format(args))
	tb_logger = SummaryWriter(args.save_path)

	# Construct Networks (Encoder, dim_loss)
	model = models.__dict__[args.arch](args.num_classes).cuda()
	print("=> created encoder '{}'".format(args.arch))
	
	toy_input = torch.zeros([5, 3, args.input_size, args.input_size]).cuda()
	arch_info = get_dim(model, toy_input, args.layers, args.c_layer)

	dim_loss = models.__dict__['DIM_Loss'](arch_info).cuda()
	classifier = models.__dict__[args.classifier](arch_info).cuda()

	# optimizer
	para_dict = itertools.chain(
					filter(lambda x: x.requires_grad, model.parameters()),
					filter(lambda x: x.requires_grad, dim_loss.parameters()),
					filter(lambda x: x.requires_grad, classifier.parameters()))
	optimizer = torch.optim.RMSprop(para_dict, lr=args.lr, alpha=0.9)

	# criterions
	crit = nn.CrossEntropyLoss().cuda()

	# optionally resume from a checkpoint
	if args.resume:
		logger.info("=> loading checkpoint '{}'".format(args.resume))
		start_epoch, best_acc = load_checkpoint(model, dim_loss, optimizer, args.resume)

	# data loading
#	dataset = McDataset(
#		  args.root, 
#		  args.source, 
#		  transform=transforms.ToTensor())

	train_dataset = torchvision.datasets.CIFAR10(root='../data', train=True, 
				download=True, transform=transforms.ToTensor())

	test_dataset = torchvision.datasets.CIFAR10(root='../data', train=False,
				download=True, transform=transforms.ToTensor())

	train_loader = torch.utils.data.DataLoader(
		  train_dataset, batch_size=args.bs,
		  num_workers=args.workers, pin_memory=True, shuffle=True)
	test_loader = torch.utils.data.DataLoader(
		  test_dataset, batch_size=args.bs,
		  num_workers=args.workers, pin_memory=True, shuffle=False)

	
	for epoch in range(start_epoch, args.epochs):
	
		end = time.time()

		# Evaluation
		acc = test(train_loader, test_loader, classifier, model, epoch, tb_logger)
	
		# saving checkpoint
		is_best_acc = acc > best_acc
		best_acc = max(acc, best_acc)
		save_checkpoint({
			  'epoch': epoch, 
			  'model': model.state_dict(), 
			  'dim_loss': dim_loss.state_dict(), 
			  'best_acc': best_acc,
			  'optimizer': optimizer.state_dict()}, 
			  is_best_acc, args.save_path + '/ckpt') 

		# training
		train(train_loader, model, dim_loss, classifier, crit, optimizer, epoch, tb_logger)


def train(loader, model, dim_loss, classifier, crit, optimizer, epoch, tb_logger):
		
	freq = args.print_freq

	batch_time = AverageMeter(freq)
	losses = AverageMeter(freq)
	top1 = AverageMeter(freq)

	logger = logging.getLogger('global_logger')

	# switch to train mode
	model.train()
	dim_loss.train()
	classifier.train()

	end = time.time()

	for i, (input_tensor, target) in enumerate(loader):
		input_var = torch.autograd.Variable(input_tensor.cuda())
		target = target.cuda()
		target_var = torch.autograd.Variable(target)

		out, [M, Y], c_vec = forward(model, input_var, args.layers, args.c_layer)
		M_fake = torch.cat((M[1:], M[0].unsqueeze(0)), dim=0)
#		Y_aug, M, M_fake = get_pair(Y, M)
		pred_out = classifier(c_vec.detach())

		loss = dim_loss(Y, M, M_fake)
		losses.update(loss.item())
		c_loss = crit(pred_out, target_var)

		prec1 = accuracy(pred_out.data, target)[0]
		top1.update(prec1.item())


		loss += c_loss
		optimizer.zero_grad()
		loss.backward(retain_graph=True)
		optimizer.step()

		# measure elapsed time
		batch_time.update(time.time() - end)
		end = time.time()

		if i % args.print_freq == 0:	
			step = epoch * len(loader) + i
			tb_logger.add_scalar('loss', losses.avg, step)
			logger.info('Epoch: [{0}/{1}][{2}/{3}]\t'
				  'Time: {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
				  'ACC: {top1.avg:.3f}\t'
				  'loss: {losses.avg:.4f}\t'.format(
					epoch, args.epochs, i, len(loader), 
					batch_time=batch_time, top1=top1, losses=losses))


def test(train_loader, test_loader, classifier, model, epoch, tb_logger):
	logger = logging.getLogger('global_logger')

	model.eval()
	classifier.eval()

	top1 = AverageMeter()

	# Forward and save predicted labels
	if args.eval_cluster:
		gnd_labels = []
		pred_labels = []
		with torch.no_grad():
			for i, (input_tensor, target) in enumerate(train_loader):
				input_var = torch.autograd.Variable(input_tensor.cuda())
				vec, _, _ = forward(model, input_var, args.layers, args.c_layer)
				_, indices = torch.max(vec, 1)
				gnd_labels.extend(target.data.numpy())
				pred_labels.extend(indices.data.cpu().numpy())
		gnd_labels = np.array(gnd_labels)
		pred_labels = np.array(pred_labels)
		
		nmi = normalized_mutual_info_score(gnd_labels, pred_labels)
		acc = clustering_acc(gnd_labels, pred_labels)
		ari = adjusted_rand_score(gnd_labels, pred_labels)
		
		logger.info('Epoch: [{0}/{1}]\t'
					'ARI: {2:.3f}\t'
					'NMI: {3:.3f}\t'
					'B_ACC: {4:.3f}\t'.format(epoch, args.epochs, ari, nmi, acc))
		step = epoch * len(train_loader)
		tb_logger.add_scalar('ARI', ari, step)
		tb_logger.add_scalar('NMI', nmi, step)
		tb_logger.add_scalar('B_ACC', acc, step)

	with torch.no_grad():
		for i, (input_tensor, target) in enumerate(test_loader):
			input_var = torch.autograd.Variable(input_tensor.cuda())
			target = target.cuda()
			_, _, c_vec = forward(model, input_var, args.layers, args.c_layer)
			pred_out = classifier(c_vec)
			prec1 = accuracy(pred_out.data, target)[0]

			top1.update(prec1.clone()[0])	

		# Logging
			
	logger.info('Epoch: [{0}/{1}]\t classifier ACC: {top1.avg:.3f}'.format(epoch, args.epochs, top1=top1))
	tb_logger.add_scalar('test_acc', top1.avg, epoch * len(train_loader))


	return top1.avg





if __name__ == '__main__':
	main()
